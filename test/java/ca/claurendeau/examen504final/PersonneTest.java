package ca.claurendeau.examen504final;

import static org.junit.Assert.assertTrue;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import ca.claurendeau.examen504final.bean.Personne;
import ca.claurendeau.examen504final.humeur.Heureux;

public class PersonneTest {

	Personne personne;
	
	
    @Before
    public void setUp() {
    	personne = new Personne(new Heureux());
    }
    
    @After
    public void exterminate() {
    	personne  = null;
    }
    
    @Test
    public void testGetPersonne() {
    	assertTrue(personne.getHumeur().equals(new Heureux().getHumeur()));
    }

}
