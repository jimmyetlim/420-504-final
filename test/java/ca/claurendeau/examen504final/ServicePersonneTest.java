package ca.claurendeau.examen504final;

import static org.junit.Assert.*;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import ca.claurendeau.examen504final.bean.Personne;
import ca.claurendeau.examen504final.humeur.Heureux;
import ca.claurendeau.examen504final.humeur.Malheureuse;
import ca.claurendeau.examen504final.humeur.Triste;
import ca.claurendeau.examen504final.service.PersonneService;

public class ServicePersonneTest {
	private final ByteArrayOutputStream outContent = new ByteArrayOutputStream();
	List<Personne> personnes;

	@Before
	public void setUp() {
		System.setOut(new PrintStream(outContent));
		personnes = new ArrayList<>();

	}

	@After
	public void exterminate() {
		personnes = null;
	}

	@Test
	public void testOutputTroisPersonne() {
		personnes.add(new Personne(new Heureux()));
		personnes.add(new Personne(new Malheureuse()));
		personnes.add(new Personne(new Triste()));
		PersonneService.justificationDeHumeurPourUnGroupe(personnes);
		assertEquals("J'ai un MacBook Pro, j'ai tout ce qu'il me faut  pour être une personne heureuse!\n"
				+ "J'ai besoin d'un MacBook Pro pour être une personne heureuse!\n"
				+ "Je fais parti des gens qui n'auront jamais de MacBook Pro\n",
				outContent.toString());
	}

	@Test
	public void testOutputHeureuse() {
		personnes.add(new Personne(new Heureux()));
		PersonneService.justificationDeHumeurPourUnGroupe(personnes);
		assertEquals("J'ai un MacBook Pro, j'ai tout ce qu'il me faut  pour être une personne heureuse!\n",
				outContent.toString());
	}

	@Test
	public void testOutputMalheureuse() {
		personnes.add(new Personne(new Malheureuse()));
		PersonneService.justificationDeHumeurPourUnGroupe(personnes);
		assertEquals("J'ai besoin d'un MacBook Pro pour être une personne heureuse!\n", outContent.toString());
	}

	@Test
	public void testOutputTriste() {
		personnes.add(new Personne(new Triste()));
		PersonneService.justificationDeHumeurPourUnGroupe(personnes);
		assertEquals("Je fais parti des gens qui n'auront jamais de MacBook Pro\n", outContent.toString());
	}

	@Test
	public void testPrintAll() {
		personnes.add(new Personne(new Triste()));
		PersonneService.printAll(personnes);
		assertEquals("Personne [humeur=triste]\n", outContent.toString());
	}

}
