package ca.claurendeau.examen504final.humeur;

public interface Humeur {
	public String toString();
	public String getHumeur();
	public void justification();
}
