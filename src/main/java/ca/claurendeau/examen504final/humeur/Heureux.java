package ca.claurendeau.examen504final.humeur;

public class Heureux implements Humeur {
    
	public final static String HEUREUSE = "heureuse";
    
	
	
	public Heureux() {
		super();
	}

	@Override
	public String getHumeur() {
		return HEUREUSE;
	}

	@Override
	public void justification() {
		System.out.print("J'ai un MacBook Pro, j'ai tout ce qu'il me faut  pour "
				+ "être une personne heureuse!\n");
	}
	
	@Override
	public String toString() {
		return HEUREUSE;
	}

}
