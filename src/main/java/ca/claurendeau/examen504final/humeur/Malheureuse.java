package ca.claurendeau.examen504final.humeur;

public class Malheureuse implements Humeur {
	
	public final static String MALHEUREUSE = "malheureuse";
	
	public Malheureuse() {
		super();
	}
	
	@Override
	public String getHumeur() {
		return MALHEUREUSE;
	}

	@Override
	public void justification() {
		System.out.print("J'ai besoin d'un MacBook Pro pour "
				+ "être une personne heureuse!\n");
	}
	
	@Override
	public String toString() {
		return MALHEUREUSE;
	}

}
