package ca.claurendeau.examen504final.humeur;

public class Triste implements Humeur {
	
	public final static String TRISTE = "triste";
	
	public Triste() {
		super();
	}
	
	@Override
	public String getHumeur() {
		return TRISTE;
	}

	@Override
	public void justification() {
		System.out.print("Je fais parti des gens qui n'auront "
				+ "jamais de MacBook Pro\n");
	}
	
	@Override
	public String toString() {
		return TRISTE;
	}

}
