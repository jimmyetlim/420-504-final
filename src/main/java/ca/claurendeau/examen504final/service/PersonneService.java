package ca.claurendeau.examen504final.service;

import java.util.List;

import ca.claurendeau.examen504final.bean.Personne;

public class PersonneService {

	public static void printAll(List<Personne> personnes) {
		personnes.stream()
                 .forEach(System.out::print);
	}

	public static void justificationDeHumeurPourUnGroupe(List<Personne> personnes) {
		for (Personne personne : personnes) {
            personne.justification();
        }
	}
}
