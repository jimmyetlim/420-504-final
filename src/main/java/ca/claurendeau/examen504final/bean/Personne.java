package ca.claurendeau.examen504final.bean;

import ca.claurendeau.examen504final.humeur.Humeur;

/**
 * 
 * Remanier le code ci-dessous de façon à ce qu'il utilise le 'State/Strategy Pattern'
 * 
 * Vous devrez nommer chacun des remaniements que vous faites ainsi que de faire un commit à chaque remaniement.
 *
 */
public class Personne {
    
    private Humeur humeur;

    public Personne(Humeur humeur) {
        this.humeur = humeur;
    }

    public String getHumeur() {
        return humeur.getHumeur();
    }
    
    
    public void setHumeur(Humeur humeur) {
		this.humeur = humeur;
	}
    
    public void justification() {
    	humeur.justification();
    }

	@Override
    public String toString() {
        return "Personne [humeur=" + humeur.getHumeur() + "]\n";
    }

    
}
